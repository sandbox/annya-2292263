(function() {
  window._pa = window._pa || {};
  var pa = document.createElement('script');
  pa.type = '<?php print $pa_type; ?>';
  pa.async =  <?php print $pa_async?>;
  pa.src = ('https:' == document.location.protocol ? 'https:' : 'http:') + "<?php print $pa_src?>";
  
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(pa, s);
})();
